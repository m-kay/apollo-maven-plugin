package com.gitlab.mkay.graphql.apollo;

import java.io.File;
import java.util.*;

import org.apache.maven.plugin.*;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.plugins.annotations.Mojo;

import com.apollographql.apollo.compiler.GraphQLCompiler;
import com.apollographql.apollo.compiler.NullableValueType;

/**
 * Mojo to execute apollo graphql compiler to generate apollo query and type classes
 */
@Mojo(name = "generate-apollo", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public class ApolloMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project.build.resources[0].directory}/schema.json")
	private File schemaInput;

	@Parameter(defaultValue = "${project.basedir}/src/main/generated")
	private File outputDir;

	@Parameter(defaultValue = "${project.groupId}")
	private String packageName;

	@Parameter(defaultValue = "JAVA_OPTIONAL")
	private NullableValueType nullableValueType;

	@Parameter
	private Map<String, String> customTypes = Collections.emptyMap();

	@Parameter(defaultValue = "true")
	private Boolean useSemanticNaming;

	@Parameter(defaultValue = "true")
	private Boolean generateModelBuilder;

	@Parameter(defaultValue = "true")
	private Boolean useJavaBeansSematicNaming;

	@Parameter(defaultValue = "true")
	private Boolean suppressRawTypesWarning;

	@Parameter(defaultValue = "false")
	private Boolean generateKotlinModels;

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		getLog().info("context: " + getPluginContext());
		getLog().info("generating apollo classes for schema " + schemaInput.getAbsolutePath());
		if(schemaInput.exists()) {
			try {
				GraphQLCompiler compiler = new GraphQLCompiler();
				compiler.write(new GraphQLCompiler.Arguments(
						schemaInput, outputDir,
						customTypes, nullableValueType,
						useSemanticNaming, generateModelBuilder, useJavaBeansSematicNaming,
						packageName, suppressRawTypesWarning, generateKotlinModels
				));
			}
			catch (Exception e){
				getLog().error(e);
			}
		}
		else {
			throw new MojoFailureException("Could not find schema file " + schemaInput.getAbsolutePath());
		}
	}
}
